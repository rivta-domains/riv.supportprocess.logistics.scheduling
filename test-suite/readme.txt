README

Testsviter f�r tj�nsteproducenter inom dom�n Tidbokning.

Till varje tj�nstekontrakt (TK) finns ett SoapUI-projekt med tester. Dokumentation finns i filen <kontraktsnamn>-doc.html som medf�ljer varje SoapUI-projekt. 
D�r finns �ven en generell beskrivning f�r installation och konfiguration av testerna.

Kataloger och projekt med �ndelsen "Mock" �r �mnade f�r testutveckling och inte test av skarpa tj�nsteproducenter eller konsumenter.